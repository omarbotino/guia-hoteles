$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toogle='popover']").popover();
    $('.carousel').carousel({
        interval: 5000
    }
    );
    $("#contacto").on('show.bs.modal',function(e){
        console.log('el modal se esta mostrando');
        $('#btnquito').removeClass('btn-outline-success');
        $('#btnquito').addClass('btn-primary');
        $('#btnquito').prop('disable',true)
    });
    $("#contacto").on('shown.bs.modal',function(e){
        console.log('el modal se mostro');
    });
    $("#contacto").on('hide.bs.modal',function(e){
        console.log('el modal se oculta');
        $('#btnquito').removeClass('btn-primary');
        $('#btnquito').addClass('btn-outline-success');
    });
    $("#contacto").on('hiden.bs.modal',function(e){
        console.log('el modal se oculto');

        $('#btnquito').prop('disable',false)
    });
});